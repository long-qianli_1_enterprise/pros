<?php

namespace We7\V217;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1580975917
* @version 2.1.7
*/

class CreateUniAccountExtraModules {

	/**
	 *  执行更新
	 */
	public function up() {
		pdo_query("CREATE TABLE IF NOT EXISTS `ims_uni_account_extra_modules` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
`uniacid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属帐号uniacid',
`modules` text NOT NULL COMMENT '应用权限组所包含的模块',
PRIMARY KEY (`id`),
KEY `uniacid` (`uniacid`) 
);");
		$ext_uni_groups = table('uni_group')->where('uniacid >', 0)->getall();

		if (!empty($ext_uni_groups)) {
			foreach($ext_uni_groups as $ext_uni_group) {
				$data = array(
					'uniacid' => $ext_uni_group['uniacid'],
					'modules' => $ext_uni_group['modules'],
				);

				table('uni_account_extra_modules')->fill($data)->save();
				table('uni_group')->where('id', $ext_uni_group['id'])->delete();
			}
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}

<?php

namespace We7\V185;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1542871361
 * @version 1.8.5
 */

class AlterUsersAddColumnNoticeSetting {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('users', 'notice_setting')) {
			pdo_query("ALTER TABLE " . tablename('users') . " ADD `notice_setting` varchar(5000) NOT NULL DEFAULT '';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		
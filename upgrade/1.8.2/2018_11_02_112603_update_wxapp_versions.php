<?php

namespace We7\V182;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1541129163
 * @version 1.8.2
 */

class UpdateWxappVersions {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('wxapp_versions', 'tominiprogram')) {
			pdo_query("ALTER TABLE " . tablename('wxapp_versions') . " ADD `tominiprogram` VARCHAR(350) NULL DEFAULT NULL COMMENT '要跳转的小程序appids';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		
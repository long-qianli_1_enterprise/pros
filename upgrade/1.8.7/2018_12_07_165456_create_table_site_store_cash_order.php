<?php

namespace We7\V187;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1544172896
 * @version 1.8.7
 */

class CreateTableSiteStoreCashOrder {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('site_store_cash_order')) {
			$table_name = tablename('site_store_cash_order');
			pdo_run(
				"CREATE TABLE {$table_name} (
					`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
					`number` char(30) NOT NULL DEFAULT '' COMMENT '订单号',
					`founder_uid` int(10) NOT NULL DEFAULT 0 COMMENT '副创始人uid',
					`order_id` int(10) NOT NULL DEFAULT 0 COMMENT '商品订单id',
					`goods_id` int(10) NOT NULL DEFAULT 0 COMMENT '商品id',
					`order_amount` decimal(10,2) NOT NULL DEFAULT 0 COMMENT '订单金额',
					`cash_log_id` int(10) NOT NULL DEFAULT 0 COMMENT '申请提现记录id',
					`status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '提现状态:1:未提现,2:申请提现,3:已拒绝,4:已核销',
					`create_time` int(10) NOT NULL DEFAULT 0,
					PRIMARY KEY (`id`),
					KEY `founder_uid` (`founder_uid`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='分销订单表';"
			);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		
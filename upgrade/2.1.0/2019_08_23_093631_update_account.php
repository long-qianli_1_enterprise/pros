<?php

namespace We7\V210;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1566524191
* @version 2.1.0
*/

class UpdateAccount {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('account', 'send_account_expire_status')) {
			$sql = "ALTER TABLE " . tablename('account') . " ADD COLUMN `send_account_expire_status` TINYINT(3) NOT NULL DEFAULT 0 COMMENT '平台到期短信提醒状态：0没发短信，1已经发送信息'";
			pdo_run($sql);
		}

		if (!pdo_fieldexists('account', 'send_api_expire_status')) {
			$sql = "ALTER TABLE " . tablename('account') . " ADD COLUMN `send_api_expire_status` TINYINT(3) NOT NULL DEFAULT 0 COMMENT '平台api到期短信提醒状态：0没发短信，1已经发送信息'";
			pdo_run($sql);
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}

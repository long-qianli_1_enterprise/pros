<?php

	namespace We7\V218;

	defined('IN_IA') or exit('Access Denied');
	/**
	 * [WeEngine System] Copyright (c) 2014 W7.CC
	 * Time: 1588062877
	 * @version 2.1.8
	 */

	class UpdateSiteTemplate {

		/**
		 *  执行更新
		 */
		public function up() {
			$default = pdo_get('modules', array('name' => 'default', 'application_type' => 2), array('mid'));
			if (empty($default)) {
				pdo_insert('modules', array('name' => 'default', 'application_type' => 2, 'type' => 0, 'title' => '微站默认模板', 'version' => '1.0.0', 'description' => '由微擎提供默认微站模板套系', 'author' => '微擎团队', 'url' => 'https://www.w7.cc', 'settings' => 0, 'isrulefields' => 0, 'issystem' => 1, 'target' => 0, 'iscard' => 0, 'wxapp_support' => 1, 'account_support' => 2, 'welcome_support' => 0, 'webapp_support' => 0, 'oauth_type' => 1, 'phoneapp_support' => 1, 'xzapp_support' => 1, 'aliapp_support' => 1, 'logo' => 'app/themes/default/preview.jpg', 'baiduapp_support' => 1, 'toutiaoapp_support' => 1, 'from' => 'cloud', 'cloud_record' => 0, 'sections' => 0));
			}
			$default_info = pdo_get('modules', array('name' => 'default', 'application_type' => 2), array('mid', 'title'));
			$site_data = pdo_getall('site_styles', array('templateid' => 0), array('id', 'name'));
			if (!empty($site_data)) {
				foreach ($site_data as $site_val) {
					pdo_update('site_styles', array('templateid' => $default_info['mid'], 'name' => '微站默认模板'), array('id' => $site_val['id']));
				}
			}
		}

		/**
		 *  回滚更新
		 */
		public function down() {


		}
	}

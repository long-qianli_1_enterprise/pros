<?php

namespace We7\V2751;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1650280898
 * @version 2.7.51
 */

class UpdateWechatField {
	/**
	 *  执行更新
	 */
	public function up() {
		if (pdo_fieldexists('wechat_news', 'thumb_media_id')) {
			pdo_run("ALTER TABLE " . tablename('wechat_news') . " CHANGE `thumb_media_id` `thumb_media_id` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片的media_id';");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {
	}
}

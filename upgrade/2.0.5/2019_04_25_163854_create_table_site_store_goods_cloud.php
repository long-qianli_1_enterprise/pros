<?php

namespace We7\V205;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1556181534
 * @version 2.0.5
 */

class CreateTableSiteStoreGoodsCloud {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('site_store_goods_cloud')) {
			$table_name = tablename('site_store_goods_cloud');
			$sql = <<<EOF
CREATE TABLE $table_name (
	`id` int(10) unsigned not null AUTO_INCREMENT,
	`cloud_id` int(10) unsigned unique not null DEFAULT '0',
	`name` varchar(100) not null DEFAULT '' COMMENT '模块标识',
	`title` varchar(100) not null DEFAULT '' COMMENT '模块名称',
	`logo` varchar(300) not null DEFAULT '',
	`wish_branch` int(10) not null DEFAULT 0 COMMENT '星愿分支',
	`is_edited` tinyint(1) not null DEFAULT 0 COMMENT '是否已编辑为商品: 1是,0否',
	`isdeleted` tinyint(1) not null DEFAULT 0,
	`branchs` varchar(6000) not null DEFAULT '' COMMENT '分支数据',
	PRIMARY KEY(`id`)
) DEFAULT CHARSET=utf8;
EOF;
			pdo_query($sql);
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		
<?php

	namespace We7\V220;

	defined('IN_IA') or exit('Access Denied');
	/**
	 * [WeEngine System] Copyright (c) 2014 W7.CC
	 * Time: 1595316458
	 * @version 2.2.0
	 */

	class UpdateAttachmentGroupField {

		/**
		 *  执行更新
		 */
		public function up() {
			if (!pdo_fieldexists('attachment_group', 'pid')) {
				pdo_query('ALTER TABLE '.tablename('attachment_group')." ADD COLUMN `pid` int(11) NOT NULL DEFAULT 0 AFTER `id`;");
			}
		}

		/**
		 *  回滚更新
		 */
		public function down() {


		}
	}

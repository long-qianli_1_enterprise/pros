<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1552556483
 * @version 1.8.8
 */

class AlterStatVisistIpSetIpBigint {

	/**
	 *  执行更新
	 */
	public function up() {
		if (pdo_fieldexists('stat_visit_ip','ip')) {
			pdo_query("ALTER TABLE " . tablename('stat_visit_ip') . " MODIFY COLUMN `ip` BIGINT(10) NOT NULL");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		
<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1546570828
 * @version 1.8.8
 */

class CreateTableCoreMenuShortcut {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_tableexists('core_menu_shortcut')) {
			$tablename = tablename('core_menu_shortcut');
			$sql = "CREATE TABLE $tablename (
				  `id` int(10) NOT NULL AUTO_INCREMENT,
				  `uid` int(10) NOT NULL DEFAULT '0',
				  `uniacid` int(10) NOT NULL DEFAULT '0',
				  `modulename` varchar(100) NOT NULL DEFAULT '' COMMENT '模块名称',
				  `displayorder` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
				  `position` varchar(100) NOT NULL DEFAULT '' COMMENT '类型或位置标识',
				  `updatetime` int(10) NOT NULL DEFAULT '0',
				  PRIMARY KEY (`id`),
				  KEY `uid` (`uid`),
				  KEY `position` (`position`)
			) DEFAULT CHARSET=utf8;";
			pdo_run($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		
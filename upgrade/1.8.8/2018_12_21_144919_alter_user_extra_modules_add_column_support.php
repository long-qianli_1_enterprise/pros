<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1545374959
 * @version 1.8.8
 */

class AlterUserExtraModulesAddColumnSupport {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('users_extra_modules', 'support')) {
			pdo_query("ALTER TABLE " . tablename('users_extra_modules') . " ADD `support` varchar(50) NOT NULL DEFAULT '' COMMENT '支持的模块类型';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		
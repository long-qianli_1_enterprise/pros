<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1540625726
 * @version 1.8.8
 */

class MigrateDataFromUsersToUsersFounderOwnUsers {

	/**
	 *  执行更新
	 */
	public function up() {
		$founder_own_users = tablename('users_founder_own_users');
		$users = tablename('users');
		$sql = <<<EOF
INSERT INTO $founder_own_users(`uid`, `founder_uid`) select `uid`, `owner_uid` from $users where `owner_uid` != 0;
EOF;
		pdo_query($sql);
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		
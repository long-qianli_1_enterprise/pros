<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1545305615
 * @version 1.8.8
 */

class UpdateModulesCloud {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('modules_cloud', 'baiduapp_support')) {
			pdo_query("ALTER TABLE " . tablename('modules_cloud') . " ADD `baiduapp_support` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否支持百度小程序';");
		}
		if(!pdo_fieldexists('modules_cloud', 'toutiaoapp_support')) {
			pdo_query("ALTER TABLE " . tablename('modules_cloud') . " ADD `toutiaoapp_support` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否支持头条小程序';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		
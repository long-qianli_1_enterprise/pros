<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1546940992
 * @version 1.8.8
 */

class AlterTableStatVisitAddColumnIpCount {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('stat_visit', 'ip_count')) {
			pdo_query("ALTER TABLE " . tablename('stat_visit') . " ADD `ip_count` int(10) NOT NULL DEFAULT 1 COMMENT 'ip访问统计';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		
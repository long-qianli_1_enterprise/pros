<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1552034981
 * @version 1.8.8
 */

class DeleteUnusedTemplateFile {

	/**
	 *  执行更新
	 */
	public function up() {
		$files = array(
			IA_ROOT . '/web/themes/2.0/common/footer-base.html',
			IA_ROOT . '/web/themes/black/common/footer-base.html',
			IA_ROOT . '/web/themes/black/common/footer.html',
			IA_ROOT . '/web/themes/classical/common/footer-base.html',
		);
		foreach ($files as $file) {
			if (file_exists($file)) {
				@unlink($file);
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		
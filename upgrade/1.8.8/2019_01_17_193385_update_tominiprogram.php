<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1545305615
 * @version 1.8.8
 */

class UpdateTominiprogram {

	/**
	 *  执行更新
	 */
	public function up() {
		pdo_run("ALTER TABLE `ims_wxapp_versions` CHANGE `tominiprogram` `tominiprogram` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;");

		$all_version = pdo_getall('wxapp_versions', array(), array('id', 'tominiprogram'));
		if (!empty($all_version)) {
			foreach($all_version as $version) {
				$tominiprogram = iunserializer($version['tominiprogram']);
				if (empty($tominiprogram) || !is_array($tominiprogram)) {
					continue;
				}
				$data = array();
				foreach($tominiprogram as $key => $item) {
					if (!is_array($item) && !empty($item)) {
						$data[$item]['appid'] = $data[$item]['app_name'] = $item;
					}
				}
				if (!empty($data)) {
					pdo_update('wxapp_versions', array('tominiprogram' => iserializer($data)), array('id' => $version['id']));
				}
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		
<?php

namespace We7\V211;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1572333573
* @version 2.1.1
*/

class CreateUsersLoginLogs {

	/**
	 *  执行更新
	 */
	public function up() {
		pdo_query("CREATE TABLE IF NOT EXISTS `ims_users_login_logs` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`uid` int(11) unsigned NOT NULL,
`ip` varchar(15) NOT NULL,
`city` varchar(256) NOT NULL DEFAULT '' COMMENT '登录地址',
`login_at` int(10) unsigned NOT NULL DEFAULT '0',
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}

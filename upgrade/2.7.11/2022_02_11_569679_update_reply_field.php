<?php

namespace We7\V2711;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1644569679
 * @version 2.7.11
 */

class UpdateReplyField {
	/**
	 *  执行更新
	 */
	public function up() {
		if (pdo_fieldexists('news_reply', 'media_id')) {
			pdo_run("ALTER TABLE " . tablename('news_reply') . " CHANGE `media_id` `media_id` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '素材的media_id';");
		}
		if (pdo_fieldexists('wxapp_reply', 'mediaid')) {
			pdo_run("ALTER TABLE " . tablename('wxapp_reply') . " CHANGE `mediaid` `mediaid` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '素材的media_id';");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {
	}
}
